<?php
/*
Plugin Name: Status de Internet - Link Speed
Plugin URI: https://maxwellpas.com.br
Description: Esse plugin é para alterar status da rede de internet de determinada região
Version: 1.0
Author: Maxwell Pereira
Author URI: https://maxwellpas.com.br
*/


if(!defined('ABSPATH'))exit;

class StatusInternetLinkspeed
{
    const TEXTDOMAIN = "status_internet_linkspeed";

    private static $instance;

    public static function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        add_action("add_meta_boxes", array($this, 'add'));
        add_action("save_post", array($this, 'save'));

        // templates
        add_action("template_include", array($this, 'add_template_linkspeed'));
        add_action('wp_enqueue_scripts', array($this, 'add_style_scripts'));
    }


    public function add_template_linkspeed($template)
    {
        if( is_page('suporte-tecnico') ){ // verifica qual single page está, no caso filmes_reviews

            if( file_exists(get_stylesheet_directory() . 'page-status_internet.php' ) ){
                return get_stylesheet_directory() . 'page-status_internet.php';

            }

            return plugin_dir_path( __FILE__ ) . 'page-status_internet.php';

        }

        return $template;
    }


    public function add_style_scripts()
    {
        wp_enqueue_style( "status_internet_linkspeed-style", plugin_dir_url( __FILE__ ) . 'status_internet_linkspeed.css' );
    }


    public static function add()
    {
        $screens = ['post'];
        foreach ($screens as $screen) {
            add_meta_box(
                self::TEXTDOMAIN . '_statusinternet_id',          // Unique ID
                'Status da Rede de Internet - Link Speed', // Box title
                [self::class, 'html'],   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }

    public static function save($post_id)
    {
        $campoSelect = self::TEXTDOMAIN . "_status_internet";
        if (array_key_exists($campoSelect, $_POST)) {
            update_post_meta($post_id,'_status_internet_value',$_POST[$campoSelect]);
        }

        $campoMensagemOnline = self::TEXTDOMAIN . "_mensagemonline_internet";
        if (array_key_exists($campoMensagemOnline, $_POST)) {
            update_post_meta($post_id,'_mensagemonline_internet_value',$_POST[$campoMensagemOnline]);
        }


        $campoMensagemOffline = self::TEXTDOMAIN . "_mensagemoffline_internet";
        if (array_key_exists($campoMensagemOffline, $_POST)) {
            update_post_meta($post_id,'_mensagemoffline_internet_value',$_POST[$campoMensagemOffline]);
        }
    }

    public static function html($post)
    {
        $valueSelect    = get_post_meta($post->ID, '_status_internet_value', true);
        $campoSelect    = self::TEXTDOMAIN . "_status_internet";

        $valueMensagemOnline         = get_post_meta($post->ID, '_mensagemonline_internet_value', true);
        $campoMensagemOnline    = self::TEXTDOMAIN . "_mensagemonline_internet";

        $valueMensagemOffline         = get_post_meta($post->ID, '_mensagemoffline_internet_value', true);
        $campoMensagemOffline    = self::TEXTDOMAIN . "_mensagemoffline_internet";
    ?>
        <table style="width: 100%;">
            <tr>
                <td style="width: 30%"><label for="<?php echo $campoSelect?>">Status Internet</label></td>
                <td>
                    <select name="<?php echo $campoSelect?>" id="<?php echo $campoSelect?>" class="form-control">
                        <option value="">Escolha aqui o status</option>
                        <option value="Online" <?php selected($valueSelect, 'Online'); ?>>Online</option>
                        <option value="Offline" <?php selected($valueSelect, 'Offline'); ?>>Offline</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width: 30%"><label for="<?php echo $campoMensagemOnline?>">Mesagem Online</label></td>
                <td>
                    <textarea rows="6" cols="100" id="<?php echo $campoMensagemOnline?>" name="<?php echo $campoMensagemOnline?>" class="form-control"><?php echo $valueMensagemOnline?></textarea>                
                </td>
            </tr>
            <tr>
                <td style="width: 30%"><label for="<?php echo $campoMensagemOffline?>">Mesagem Offline</label></td>
                <td>
                    <textarea rows="6" cols="100" id="<?php echo $campoMensagemOffline?>" name="<?php echo $campoMensagemOffline?>" class="form-control"><?php echo $valueMensagemOffline?></textarea>                
                </td>
            </tr>
        </table>
    <?php


    }
}

StatusInternetLinkspeed::getInstance();




