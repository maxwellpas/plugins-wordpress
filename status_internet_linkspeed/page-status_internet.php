<?php
get_header();


function showStatus($status, $mens)
{

}
?>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<h1 class="main_title"><?php the_title(); ?></h1>
			<?php
			$args = array('category_name' => 'suporte-tecnico', 'posts_per_page' => -1);
        	$loop = new WP_Query($args);

        	

	        $k = 1;
	        ?>
	        <table class="status-internet">
	        	<thead>
		        	<tr>
		        		<td>Bairro</td>
		        		<td>Observação</td>
		        		<td>Status</td>
		        	</tr>
	        	</thead>
				<tbody>
	        	<?php 
	        	while ($loop->have_posts()) : $loop->the_post();
	        		$status 	= get_post_meta($post->ID, '_status_internet_value', true) ;
		        	$msgOnline 	= get_post_meta($post->ID, '_mensagemonline_internet_value', true) ;
		        	$msgOffline = get_post_meta($post->ID, '_mensagemoffline_internet_value', true) ;
        		?>
		        	<tr>
		        		<td><?php the_title(); ?></td>
		        		<td><?php the_content(); ?></td>
		        		<td>
		        			<a href="#" data-toggle="tooltip" title="<?php echo ($status == "Online") ? $msgOnline : $msgOffline?>">
		        				<?php echo $status?>
		        			</a>
	        			</td>
		        	</tr>
		        <?php
		        $k++;
		        endwhile;
		        ?>
	        	</tbody>
	        </table>
		</div>
	</div>
</div>

<?php
//get_post_meta($post->ID, '_status_internet_value', true)
//echo get_post_meta($post->ID, '_mensagemonline_internet_value', true);
//get_post_meta($post->ID, '_mensagemoffline_internet_value', true)
get_footer();
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $('.status-internet').DataTable({
	    	"oLanguage": {
             "sProcessing": "Aguarde enquanto os dados sÃ£o carregados ...",
             "sLengthMenu": "Mostrar _MENU_ registros por pagina",
             "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
             "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
             "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
             "sInfoFiltered": "",
             "sSearch": "Procurar",
             "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Pr&oacute;ximo",
                "sLast":     "&Uacute;ltimo"
             }
          },
         "aaSorting": []
	    });

	    $('[data-toggle="tooltip"]').tooltip();   

	});
</script>