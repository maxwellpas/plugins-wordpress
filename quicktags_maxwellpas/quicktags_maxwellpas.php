<?php
/*
Plugin Name: Quick Tags - Projeto Maxwellpas
Plugin URI: http://exemplo.com
Description: Plugin desenvolvido para inserir quicktag dos projetos do site maxwellpas.com.br
Version: 1.0
Author: Maxwell Pereira
Author URI: https://www.maxwellpas.com.br
Text Domain: quicktags_maxwellpas
License: GPL2
*/
if(!defined('ABSPATH'))exit;
class Quicktags_Maxwellpas {
    private static $instance;

    public static function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    private function __construct() 
    {
        add_action('admin_print_footer_scripts',array($this,'quicktags_maxwellpas'));
    }

    public function buscaTemplate()
    {
        $html = "";
        $html .= '<p>Uma breve descrição\nEsse projeto foi feito com parceria do designer <a href="aaaa" target="_blank">bbbb</a></p>\n<ul>\n\t<li><strong>Website:</strong> <a href="ccc" target="_blank">ddd</a></li>\n\t<li><strong>Tecnlogia:</strong> PHP / JS / CSS3</li>\n\t<li><strong>CMS:</strong> Wordpress</li>\n</ul>';


        return $html;
    }


    public function quicktags_maxwellpas()
    {

        if(wp_script_is('quicktags')){
?>
            <script type="text/javascript">
                function getSele(){
                    var txtarea = document.getElementById("content");
                    var start   = txtarea.selectionStart;
                    var finish  = txtarea.selectionEnd;
                    return txtarea.value.substring(start,finish);
                }

                QTags.addButton('quicktags_maxwellpas','Template - HTML do projeto',getTemplate);

                function getTemplate()
                {
                    var selected_text = getSele();
                    QTags.insertContent('<?php echo $this->buscaTemplate();?>');
                }
            </script>

<?php

        }

    }

}

Quicktags_Maxwellpas::getInstance();