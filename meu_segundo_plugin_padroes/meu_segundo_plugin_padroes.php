<?php
/**
* Plugin Name: Personalizar Painel Padrão Singleton
* Plugin URI: http://www.exemplo.com.br
* Description: Plugin para personalizar painel
* Version: 1.0
* Author: Maxwell Pereira
* Author URI: https://www.maxwellpas.com.br
* Text Domain: meu-segundo-plugin-padroes
* Licence: GPLv2
**/


class SegundoPlugin{

	private static $instance;
    const TEXT_DOMAIN = "meu-segundo-plugin-padroes";

	public static function getInstance()
	{
		if(self::$instance == NULL){
			self::$instance = new self();

		}

	}


	private function __construct()
	{
		/** DESATIVAR A ACTION WELCOME PAINEL **/
		remove_action('welcome_panel', 'wp_welcome_panel');
		// ADICIONANDO A PERSONALIZADA
		add_action('welcome_panel', array($this, 'welcome_panel') );


		add_action('admin_enqueue_scripts', array($this, 'addCss') );

        add_action('init', array($this, 'meusegundoplugin_load_textdomain') );
	}


    public function meusegundoplugin_load_textdomain()
    {
        load_plugin_textdomain(self::TEXT_DOMAIN, false, dirname(plugin_basename(__FILE__)));
    }


	/**
	* Usando CSS
	*/
	function addCss()
	{
		wp_register_style(self::TEXT_DOMAIN, plugin_dir_url(__FILE__) . 'css/meu-segundo-plugin-padroes.css' );
		wp_enqueue_style(self::TEXT_DOMAIN);

	}


	public function welcome_panel()
	{
	?>
		<div class="welcome-panel-content">
			<h3><?php _e("Seja bem vindo ao Painel Administrativo Customizado", self::TEXT_DOMAIN) ?></h3>
			<p><?php _e("Siga-nos nas redes sociais", self::TEXT_DOMAIN) ?></p>
			<div class="icons">
				<a href="#"><?php _e("Facebook - maxwellpas",self::TEXT_DOMAIN) ?></a><br />
				<a href="#"><?php _e("Twiiter - maxwellpas", self::TEXT_DOMAIN) ?></a><br />
				<a href="#"><?php _e("Linkedin - maxwellpas", self::TEXT_DOMAIN) ?></a><br />
			</div>
			<br />
		</div>

	<?php
	}

}

SegundoPlugin::getInstance();





