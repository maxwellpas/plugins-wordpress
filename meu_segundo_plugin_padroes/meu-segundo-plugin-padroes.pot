#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-05 22:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. Name of the plugin
msgid "Personalizar Painel Padrão Singleton"
msgstr ""

#. Description of the plugin
msgid "Plugin para personalizar painel"
msgstr ""

#. URI of the plugin
msgid "http://www.exemplo.com.br"
msgstr ""

#. Author of the plugin
msgid "Maxwell Pereira"
msgstr ""

#. Author URI of the plugin
msgid "https://www.maxwellpas.com.br"
msgstr ""
