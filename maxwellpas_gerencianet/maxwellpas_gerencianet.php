<?php
/*
Plugin Name: Maxwellpas Gerencianet
Plugin URI: http://exemplo.com
Description: Plugin para gerenciar meios de pagamentos da gerencianet
Version: 1.0
Author: Maxwell Periera
Author URI: http://maxwellpas.com.br
Text Domain: maxwellpas-gerencianet
License: GPL2
*/

require_once (dirname(__FILE__) . "/vendor/autoload.php"); // caminho relacionado a SDK

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class Maxwellpas_Gerencianet {

	private static $instance;

	public static function getInstance()
	{
		if (self::$instance == NULL) {
			self::$instance = new self();
		}

		return self::$instance;
	}


	private function __construct()
	{
		// add_action('init', array($this, 'registrar_boleto'));
		add_action("template_include", array($this, "add_template_listagerencianet"));

	}


	public function add_template_listagerencianet( $template )
	{
		if( is_page("gerencianet") ){

			if( file_exists( get_stylesheet_directory() . "page/page-maxwellpas_gerencianet.php" ) ){
				return get_stylesheet_directory() . "page/page-maxwellpas_gerencianet.php";

			}

			return plugin_dir_path(__FILE__) . "page/page-maxwellpas_gerencianet.php";
		}

		return $template;

	}


	public function registrar_boleto()
	{


		// $clientId = 'Client_Id_d944046dc5f25df291ac0ece7683676b56caf32c'; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
		// $clientSecret = 'Client_Secret_08d35645f61cf0cf5374fa0466f24d02aa59aac2'; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)

		// $options = [
		//   'client_id' => $clientId,
		//   'client_secret' => $clientSecret,
		//   'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
		// ];

		// $charge_id = 7;

		// // $charge_id refere-se ao ID da transação gerada anteriormente
		// $params = [
		//   'id' => $charge_id
		// ];

		// $customer = [
		//   'name' => 'Gorbadoc Oldbuck', // nome do cliente
		//   'cpf' => '94271564656' , // cpf válido do cliente
		//   'phone_number' => '5144916523' // telefone do cliente
		// ];

		// $bankingBillet = [
		//   'expire_at' => '2018-12-12', // data de vencimento do boleto (formato: YYYY-MM-DD)
		//   'customer' => $customer
		// ];

		// $payment = [
		//   'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
		// ];

		// $body = [
		//   'payment' => $payment
		// ];

		// try {
		//     $api = new Gerencianet($options);
		//     $charge = $api->payCharge($params, $body);

		//     print_r($charge);
		// } catch (GerencianetException $e) {
		//     print_r($e->code);
		//     print_r($e->error);
		//     print_r($e->errorDescription);
		// } catch (Exception $e) {
		//     print_r($e->getMessage());
		// }


	}




}

Maxwellpas_Gerencianet::getInstance();

