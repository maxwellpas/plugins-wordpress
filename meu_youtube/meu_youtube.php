<?php

/*Plugin Name: Meu Youtube
Plugin URI: http://exemplo.com
Description: Plugin desenvolvido para informar o canal do youtube
Version: 1.0
Author: Maxwell Periera
Author URI: http://maxwellpas.com.br
Text Domain: meu-youtube
License: GPL2
*/

class Meu_youtube {

	private static $instance;

	public static function getInstance()
	{
		if (self::$instance == NULL) {
			self::$instance = new self();
		}

		return self::$instance;
	}


	private function __construct()
	{
		add_shortcode( 'youtube', array($this, 'youtube') );

	}


	public function youtube( $parametros )
	{
		$a 		= shortcode_atts( array('canal' => ''), $parametros);
		$canal 	= $a['canal'];//GoogleDevelopers

		return '<script src="https://apis.google.com/js/platform.js"></script>
				<div class="g-ytsubscribe" data-channel="'.$canal.'" data-layout="default" data-count="default"></div>';
	}


}

Meu_youtube::getInstance();


/*Atualizar email admin*/
// update_option('admin_email','eltonoliveirape@gmail.com',false);
/*Adicionar option*/
//add_option("meupluginteste","Valor de Teste"); 

// delete_option('meupluginteste');
// echo "<h1> ".get_option('blogname')." aquiiiii é o blogname </h1><br>";
// echo "<h1>".get_option('admin_email')."</h1>";