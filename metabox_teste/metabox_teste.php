<?php

/*Plugin Name: Esse é modelo
Plugin URI: http://exemplo.com
Description: Plguin de teste de metabox
Version: 1.0
Author: Maxwell Periera
Author URI: http://maxwellpas.com.br
Text Domain: status_internet_linkspeed
License: GPL2
*/

if(!defined('ABSPATH'))exit;

class StatusInternetLinkspeed
{
    const TEXTDOMAIN = "status_internet_linkspeed";

    private static $instance;

    public static function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        add_action("add_meta_boxes", array($this, 'add'));
        add_action("save_post", array($this, 'save'));
    }


    public static function add()
    {
        $screens = ['post'];
        foreach ($screens as $screen) {
            add_meta_box(
                self::TEXTDOMAIN . '_statusinternet_id',          // Unique ID
                'Status da Rede de Internet - Link Speed', // Box title
                [self::class, 'html'],   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }

    public static function save($post_id)
    {
        $campoSelect = self::TEXTDOMAIN . "_status_internet";
        if (array_key_exists($campoSelect, $_POST)) {
            update_post_meta($post_id,'_status_internet_value',$_POST[$campoSelect]);
        }
    }

    public static function html($post)
    {
        $value = get_post_meta($post->ID, '_status_internet_value', true);
        $campoSelect = self::TEXTDOMAIN . "_status_internet";
    ?>
        <table style="width: 40%;">
            <tr>
                <td><label for="<?php echo $campoSelect?>">Status Internet</label></td>
                <td>
                    <select name="<?php echo $campoSelect?>" id="<?php echo $campoSelect?>" class="form-control">
                        <option value="">Escolha aqui o status</option>
                        <option value="Online" <?php selected($value, 'Online'); ?>>Online</option>
                        <option value="Offline" <?php selected($value, 'Offline'); ?>>Offline</option>
                    </select>
                </td>
            </tr>
        </table>


    <?php
    }
}

StatusInternetLinkspeed::getInstance();
