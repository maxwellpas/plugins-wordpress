<?php

/**
*
*/
class Meu_widget extends WP_Widget
{

	public function __construct()
	{
		// parent::WP_Widget(false, $name = "Visite minhas redes sociais!:)"); // jeito antigo de fazer o plugin
		parent::__construct(false, $name = "Visite minhas redes sociais!:)");
	}


	public function widget($args, $instance)
	{
		extract($args);

		$title 			= apply_filters('widget_title', $instance['title']);
		$urlFacebook 	= $instance['urlFacebook'];
		$urlTwitter		= $instance['urlTwitter'];
		$urlInstagram 	= $instance['urlInstagram'];
		$urlYoutube 	= $instance['urlYoutube'];

		$html = "";

		$html .= $before_widget;

		if(!empty($title)){

			$html .= "<h2 class='widget-title'>". $title . "</h2>";

			$html .= '<a href="'.$urlFacebook.'" target="_blank"><img src="'.plugin_dir_url( __FILE__ ).'../images/face.png" alt="Facebook" width="50px" /></a>';
			$html .= '<a href="'.$urlTwitter.'" target="_blank"><img src="'.plugin_dir_url( __FILE__ ).'../images/twitter.png" alt="Twitter" width="50px" /></a>';
			$html .= '<a href="'.$urlInstagram.'" target="_blank"><img src="'.plugin_dir_url( __FILE__ ).'../images/insta.png" alt="Instagram" width="50px" /></a>';
			$html .= '<a href="'.$urlYoutube.'" target="_blank"><img src="'.plugin_dir_url( __FILE__ ).'../images/youtube.png" alt="You Tube" width="50px" /></a>';
		}

		echo $html .= $after_widget;

	}


	public function update($new_instance, $old_instance)
	{
		$old_instance['title'] 			= wp_strip_all_tags($new_instance['title']);
		$old_instance['urlFacebook'] 	= wp_strip_all_tags($new_instance['urlFacebook']);
		$old_instance['urlTwitter'] 	= wp_strip_all_tags($new_instance['urlTwitter']);
		$old_instance['urlInstagram'] 	= wp_strip_all_tags($new_instance['urlInstagram']);
		$old_instance['urlYoutube'] 	= wp_strip_all_tags($new_instance['urlYoutube']);

		return $old_instance;

	}

	public function form($instance)
	{

		$title 			= esc_attr($instance['title']);
		$urlFacebook 	= esc_attr($instance['urlFacebook']);
		$urlTwitter 	= esc_attr($instance['urlTwitter']);
		$urlInstagram 	= esc_attr($instance['urlInstagram']);
		$urlYoutube 	= esc_attr($instance['urlYoutube']);

		?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"> <?php _e('Título'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title ?>" /> 
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('urlFacebook'); ?>"> <?php _e('Facebook'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('urlFacebook'); ?>" name="<?php echo $this->get_field_name('urlFacebook'); ?>" value="<?php echo $urlFacebook ?>" /> 
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('urlTwitter'); ?>"> <?php _e('Twitter'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('urlTwitter'); ?>" name="<?php echo $this->get_field_name('urlTwitter'); ?>" value="<?php echo $urlTwitter ?>" /> 
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('urlInstagram'); ?>"> <?php _e('Instagram'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('urlInstagram'); ?>" name="<?php echo $this->get_field_name('urlInstagram'); ?>" value="<?php echo $urlInstagram ?>" /> 
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('urlYoutube'); ?>"> <?php _e('Youtube'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('urlYoutube'); ?>" name="<?php echo $this->get_field_name('urlYoutube'); ?>" value="<?php echo $urlYoutube ?>" /> 
		</p>

		<?php

	}
}