<?php
/**
* Plugin Name: Filmes Reviews
* Plugin URI: http://www.exemplo.com
* Description: Plugin desenvolvido para reviews em vídeos
* Version: 1.0
* Author: Maxwell Pereira
* Author URI: http://www.maxwellpas.com.br
* Text Domain: filmes-reviews
* License: GPL2
*/

require_once dirname(__FILE__) . '/lib/class-tgm-plugin-activation.php';

class FilmesReviews{

        private static $instance;

        /** CONSTANTES **/
        const TEXT_DOMAIN = "filmes-reviews";
        const PLUGIN_NAME = "Filme Review";
        const PLUGIN_NAME_PLURAL = "Filmes Reviews";
        const DESCRIPTION = "Plugin desenvolvido para reviews em vídeos";
        const FIELD_PREFIX = "filrev_";


        public function __construct()
        {
            add_action( 'init', 'FilmesReviews::register_post_type' );
            add_action( 'tgmpa_register', 'FilmesReviews::check_required_plugins' );
            add_action( 'init', 'FilmesReviews::filmesReviewsLoadTextDomain' ); // registra internacionalização
            add_action( 'init', 'FilmesReviews::register_taxonomies' ); // registra categorias
            add_filter( 'rwmb_meta_boxes', 'FilmesReviews::metabox_custom_fields' ); // registra os metaboxs

            /** TEMPLATE CUSTOMIZADO **/
            add_action('template_include', array($this, 'add_cpt_template'));
            add_action('wp_enqueue_scripts', array($this, 'add_style_scripts'));

        }


        /**
        * INTERNACIONALIZAÇÃO
        **/
        public static function filmesReviewsLoadTextDomain()
        {
            load_plugin_textdomain( self::TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) );
        }

        public static function getInstance()
        {
            if(self::$instance == NULL){
                self::$instance = new Self();

            }

            return self::$instance;
        }

        public static function register_post_type()
        {
            $labels = array(
                'name'                  => _x( self::PLUGIN_NAME_PLURAL, 'Post type general name', self::TEXT_DOMAIN ),
                'singular_name'         => _x( self::PLUGIN_NAME, 'Post type singular name', self::TEXT_DOMAIN ),
                'menu_name'             => _x( self::PLUGIN_NAME_PLURAL, 'Admin Menu text', self::TEXT_DOMAIN ),
                'name_admin_bar'        => _x( self::PLUGIN_NAME, 'Adicionar novo on Toolbar', self::TEXT_DOMAIN ),
                'add_new'               => __( 'Adicionar novo', self::TEXT_DOMAIN ),
                'add_new_item'          => __( 'Adicionar novo '.self::PLUGIN_NAME, self::TEXT_DOMAIN ),
                'new_item'              => __( 'Novo '.self::PLUGIN_NAME, self::TEXT_DOMAIN ),
                'edit_item'             => __( 'Editar '.self::PLUGIN_NAME, self::TEXT_DOMAIN ),
                'view_item'             => __( 'Visualizar '.self::PLUGIN_NAME, self::TEXT_DOMAIN ),
                'all_items'             => __( 'Todos '. self::PLUGIN_NAME_PLURAL, self::TEXT_DOMAIN ),
                'search_items'          => __( 'Procurar '. self::PLUGIN_NAME_PLURAL, self::TEXT_DOMAIN ),
                'parent_item_colon'     => __( 'Parent '.self::PLUGIN_NAME_PLURAL.':', self::TEXT_DOMAIN ),
                'not_found'             => __( 'Não '.self::PLUGIN_NAME_PLURAL.' encontrado.', self::TEXT_DOMAIN ),
                'not_found_in_trash'    => __( 'Não '.self::PLUGIN_NAME_PLURAL.' encontrado na lixeira.', self::TEXT_DOMAIN ),
                'featured_image'        => _x( 'Imagem da capa ' . self::PLUGIN_NAME, 'Substitui a frase "Imagem destacada" por esse tipo de publicação. Adicionado em 4.3', self::TEXT_DOMAIN ),
                'set_featured_image'    => _x( 'Definir imagem da capa', 'Substitui a frase "Definir imagem destacada" para este tipo de publicação. Adicionado em 4.3', self::TEXT_DOMAIN ),
                'remove_featured_image' => _x( 'Remover imagem da capa', 'Substitui a frase "Remover imagem destacada" por esse tipo de publicação. Adicionado em 4.3', self::TEXT_DOMAIN ),
                'use_featured_image'    => _x( 'Usar como imagem da capa', 'Substitui a frase "Usar como imagem destacada" para este tipo de publicação. Adicionado em 4.3', self::TEXT_DOMAIN ),
                'archives'              => _x( self::PLUGIN_NAME . ' arquivos', 'A etiqueta de arquivo do tipo de publicação usada nos menus de navegação. Padrão "Post Archives”. Added in 4.4', self::TEXT_DOMAIN ),
                'insert_into_item'      => _x( 'Inserir na '.self::PLUGIN_NAME, 'Substitui a frase "Inserir na publicação" / "Inserir na página" (usada ao inserir a mídia em uma publicação). Adicionado em 4.4', self::TEXT_DOMAIN ),
                'uploaded_to_this_item' => _x( 'Carregar para ' . self::PLUGIN_NAME, 'Substitui a frase "Carregado para esta publicação" / "Carregado para esta página" (usado ao visualizar mídia anexada a uma publicação). Adicionado em 4.4', self::TEXT_DOMAIN ),
                'filter_items_list'     => _x( 'Lista de '.self::PLUGIN_NAME_PLURAL.' filtro', 'Texto do leitor de tela para o cabeçalho dos links do filtro na tela de listagem do tipo de publicação. Padrão "Lista de mensagens de filtro" / "Lista de páginas de filtro". Adicionado em 4.4', self::TEXT_DOMAIN ),
                'items_list_navigation' => _x( ''.self::PLUGIN_NAME_PLURAL.' Lista de mensagens', 'Texto do leitor de tela para o cabeçalho da paginação na tela de listagem do tipo de publicação. Padrão "Navegação na lista de mensagens" / "Navegação na lista de páginas". Adicionado em 4.4', self::TEXT_DOMAIN ),
                'items_list'            => _x( ''.self::PLUGIN_NAME_PLURAL.' lista', 'Texto do leitor de tela para o título da lista de itens na tela de listagem de postagem. Padrão "Lista de mensagens" / "Lista de páginas". Adicionado em 4.4', self::TEXT_DOMAIN ),
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => self::PLUGIN_NAME ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => true,
                'menu_position'      => 3,
                'menu_icon' => 'dashicons-format-video',
                'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' ),
            );
            register_post_type('filmes_reviews', $args);

        }


        /**
         *  Metabox
         */
        public static function metabox_custom_fields(){

            $meta_boxes[] = array(

            'id'        => 'data_filme',
            'title'     => __('Informações Adicionais', self::PLUGIN_NAME),
            'pages'     => array('filmes_reviews','post'),
            'context'   => 'normal',
            'priority'  => 'high',
            'fields'    => array(
                    array(
                        'name' => __('Ano de laçamento', self::PLUGIN_NAME),
                        'desc' => __('Ano que o filme foi lançano', self::PLUGIN_NAME),
                        'id'   => self::FIELD_PREFIX.'filme_ano',
                        'type' => 'number',
                        'std'  => date('Y'),
                        'min'  => '1880',
                    ),
                    array(
                        'name' => __('Diretor', self::PLUGIN_NAME),
                        'desc' => __('Quem dirigiu o filme', self::PLUGIN_NAME),
                        'id'   => self::FIELD_PREFIX.'filme_dirigiu',
                        'type' => 'text',
                        'std' => '',
                    ),
                    array(
                        'name' => 'Site',
                        'desc' => 'Link do site do filme',
                        'id'   => self::FIELD_PREFIX.'filme_site',
                        'type' => 'url',
                        'std'  => '',
                    ),

                )

            );

            $meta_boxes[] = array(

            'id'        => 'review_data',
            'title'     => __('Filme Review','filmes-reviews'),
            'pages'     => array('filmes_reviews'),
            'context'   => 'side',
            'priority'  => 'high',
            'fields'    => array(

                  array(
                    'name' => __('Rating','filmes-reviews'),
                    'desc' => __('Em uma escala de 1 - 10 , sendo que 10 é a melhor nota','filmes-reviews'),
                    'id'   => self::FIELD_PREFIX.'review_rating',
                    'type' => 'select',
                    'options' => array(

                        '' => __('Avalie Aqui','filmes-reviews'),
                        1  => __('1 - Gostei um pouco','filmes-reviews'),
                        2  => __('2 - Eu gostei mais ou menos','filmes-reviews'),
                        3  => __('3 - Não recomendo','filmes-reviews'),
                        4  => __('4 - Deu pra assistir tudo','filmes-reviews'),
                        5  => __('5 - Filme decente','filmes-reviews'),
                        6  => __('6 - Filme legal','filmes-reviews'),
                        7  => __('7 - Legal, recomendo','filmes-reviews'),
                        8  => __('8 - O meu favorito','filmes-reviews'),
                        9  => __('9 - Amei um dos meus melhores filmes','filmes-reviews'),
                        10 => __('10 - O melhor filme de todos os tempos, recomendo!!','filmes-reviews'),

                      ),  
                      'std' => '',
                  ),
                )
            );
            
            return $meta_boxes;
        }


        /**
        * Categorias
        **/
        public static function register_taxonomies()
        {
            register_taxonomy(
                'tipo_filmes',
                "filmes_reviews",
                array(
                    'labels'        => array(
                        'name' => __( 'Filmes Tipos'),
                        'singular_name' => __( 'Filmes Tipos'),
                    ),
                    'rewrite'      => array( 'slug' => "tipos-filmes" ),
                    'hierarchical' => true,
                    'public' => true,
                )
            );
        }


        /**
        * checar plugins de terceiros requiridos
        **/
        public static function check_required_plugins()
        {
            $plugins = array(
                array(
                    'name' => 'Meta Box',
                    'slug' => "meta-box" ,
                    'required' => true,
                    'force_activation' => true,
                    'force_desactivation' => false,
                ),
            );

            $config = array(
                'domain'           => self::TEXT_DOMAIN,                 // Unique ID for hashing notices for multiple instances of TGMPA.
                'default_path' => '',                      // Default absolute path to bundled plugins.
                'menu'         => 'install-required-plugins', // Menu slug.
                'parent_slug'  => 'plugins.php',            // Parent menu slug.
                'capability'   => 'update_plugins',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
                'has_notices'  => true,                    // Show admin notices or not.
                'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
                'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
                'is_automatic' => false,                   // Automatically activate plugins after installation or not.
                'message'      => '',                      // Message to output right before the plugins table.
                'strings'          => array(
                    'page_title'                      => __( 'Instalar plugins requeridos', self::TEXT_DOMAIN ),
                    'menu_title'                      => __( 'Instalar Plugins', self::TEXT_DOMAIN),
                    'installing'                      => __( 'Instalando Plugin: %s', self::TEXT_DOMAIN),
                    'oops'                            => __( 'Algo deu errado com a API do plug-in.', self::TEXT_DOMAIN ),
                    'notice_can_install_required'     => _n_noop( 'O Comentário do plugin Filmes Reviews depende do seguinte plugin:%1$s.', 'Os Comentários do plugin Filmes Reviews depende dos seguintes plugins:%1$s.' ),
                    'notice_can_install_recommended'  => _n_noop( 'O plugin Filmes review recomenda o seguinte plugin: %1$s.', 'O plugin Filmes review recomenda os seguintes plugins: %1$s.' ),
                    'notice_cannot_install'           => _n_noop( 'Desculpe, mas você não possui as permissões corretas para instalar o plugin %s. Entre em contato com o administrador deste site para obter ajuda para obter o plugin instalado.', 'Desculpe, mas você não tem as permissões corretas para instalar os plugins %s. Entre em contato com o administrador deste site para obter ajuda para obter os plugins instalados.' ),
                    'notice_can_activate_required'    => _n_noop( 'O seguinte plugin necessário está atualmente inativo: %1$s.', 'Os seguintes plugins necessários estão atualmente inativos: %1$s.' ),
                    'notice_can_activate_recommended' => _n_noop( 'O seguinte plugin recomendado está atualmente inativo: %1$s.', 'Os seguintes plugins recomendados estão atualmente inativos: %1$s.' ),
                    'notice_cannot_activate'          => _n_noop( 'Desculpe, mas você não possui as permissões corretas para ativar o plugin %s. Entre em contato com o administrador deste site para obter ajuda para obter o plugin ativado.', 'Desculpe, mas você não possui as permissões corretas para ativar os plugins %s. Entre em contato com o administrador deste site para obter ajuda sobre como ativar os plugins.' ),
                    'notice_ask_to_update'            => _n_noop( 'O seguinte plug-in precisa ser atualizado para sua versão mais recente para garantir a máxima compatibilidade com este tema: %1$s.', 'Os seguintes plugins precisam ser atualizados para sua versão mais recente para garantir a máxima compatibilidade com este tema: %1$s.' ),
                    'notice_cannot_update'            => _n_noop( 'Desculpe, mas você não tem as permissões corretas para atualizar o plugin %s. Entre em contato com o administrador deste site para obter ajuda para obter o plugin atualizado.', 'Desculpe, mas você não tem as permissões corretas para atualizar os plugins %s. Entre em contato com o administrador deste site para obter ajuda sobre como atualizar os plugins.' ),
                    'install_link'                    => _n_noop( 'Comece a instalação de plug-in', 'Comece a instalação dos plugins' ),
                    'activate_link'                   => _n_noop( 'Ativar o plugin instalado', 'Ativar os plugins instalados' ),
                    'return'                          => __( 'Voltar parapara os plugins requeridos instalados', self::TEXT_DOMAIN ),
                    'plugin_activated'                => __( 'Plugin ativado com sucesso.', self::TEXT_DOMAIN ),
                    'complete'                        => __( 'Todos os plugins instalados e ativados com sucesso. %s', self::TEXT_DOMAIN ),
                    'nag_type'                        => 'updated',
                  )
                );

            tgmpa( $plugins, $config );
        }


        public function add_cpt_template($template)
        {
            if( is_singular('filmes_reviews') ){ // verifica qual single page está, no caso filmes_reviews

                if( file_exists(get_stylesheet_directory() . 'single-filme_reviews.php' ) ){
                    return get_stylesheet_directory() . 'single-filme_reviews.php';

                }

                return plugin_dir_path( __FILE__ ) . 'single-filme_reviews.php';

            }

            return $template;
        }


        public function add_style_scripts()
        {
            wp_enqueue_style( "filme-review-style", plugin_dir_url( __FILE__ ) . 'filmes_reviews.css' );
        }


        /**
        *   Para montar as regras novamentes, princpipalmente para links permanentes
        **/
        public static function activate()
        {
            self::register_post_type();
            self::register_taxonomies();
            flush_rewrite_rules();

        }


}


FilmesReviews::getInstance();

register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_activation_hook( __FILE__, 'FilmesReviews::activate' );

